package fr.univlille.iut.r3_04.utils;

public interface Observer {
	public void update(Subject subj);

	public void update(Subject subj, Object data);
}
