package fr.univlille.iut.r3_04;

import fr.univlille.iut.r3_04.model.Bassin;
import fr.univlille.iut.r3_04.model.Bateau;
import fr.univlille.iut.r3_04.model.EBateau;
import fr.univlille.iut.r3_04.view.BassinView;

public class Jeu {

	public static void main(String[] args) {
		Bassin bassin = new Bassin(10, 10);

		bassin.ajouterBateau( new Bateau(EBateau.CROISEUR, 2, 3, true, bassin));
		bassin.ajouterBateau( new Bateau(EBateau.ESCORTEUR, 5, 7, false, bassin));
		bassin.ajouterBateau( new Bateau(EBateau.SOUS_MARIN, 3, 6, false, bassin));

		new BassinView( bassin, false);
	}

}
