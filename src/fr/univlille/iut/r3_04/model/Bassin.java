package fr.univlille.iut.r3_04.model;

import fr.univlille.iut.r3_04.utils.Subject;

public class Bassin extends Subject{

	protected int hauteur;  // largeur (nb colonnes) du bassin
	protected int largeur;  // hauteur (nb ligne) du bassin
	protected Flotte flotte;  // les bateaux dans le bassin
	protected Case[][] cases;
	protected ECoup dernierCoup;  // indication du resultat du dernier coup

	public Bassin(int hauteur, int largeur) {
		super();
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.flotte = new Flotte();
		dernierCoup = ECoup.DEBUT;

		creerCases();
	}

	private void creerCases() {
		cases = new Case[largeur][hauteur];
		for (int x=0; x < largeur; x++) {
			for (int y=0; y < hauteur; y++) {
				cases[x][y] = new Case(x, y, this);
			}
		}
	}

	public int getHauteur() {
		return hauteur;
	}

	public int getLargeur() {
		return largeur;
	}

	public Case getCase(int i, int j) {
		return cases[i][j];
	}

	public void tir(int x, int y) {
		if ( (x < 1) || (x > largeur) || (y < 1) || (y > hauteur)) {
			dernierCoup = ECoup.ERREUR;
		}
		else {
			switch (getCase(x, y).getEtat()) {
			case BATEAU: dernierCoup = coupMouche(x,y); break;
			case EAU : dernierCoup = ECoup.A_L_EAU; break;
			case TOUCHE : /* rien a faire */ break;
			}
		}
		notifyObservers(dernierCoup);
	}

	private ECoup coupMouche(int x, int y) {
		getCase(x, y).setEtat(ECase.TOUCHE);

		Bateau bateau = flotte.bateauA(x, y);
		if (bateau != null) {
			if (bateau.estDetruit()) {
				if (flotte.tousCoules()) {
					return ECoup.FINI;
				}
				else {
					return ECoup.COULE;
				}
			}
			else {
				return ECoup.TOUCHE;
			}
		}

		return ECoup.ERREUR;  // ne devrait pas arrive
	}

	public ECoup getDernierCoup() {
		return dernierCoup;
	}

	public void ajouterBateau(Bateau bateau) {
		flotte.ajouterBateau(bateau);
	}

	public Flotte getFlotte() {
		return flotte;
	}

}
