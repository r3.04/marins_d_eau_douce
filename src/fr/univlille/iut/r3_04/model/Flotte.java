package fr.univlille.iut.r3_04.model;

import java.util.ArrayList;
import java.util.List;

public class Flotte {
	private List<Bateau> bateaux;

	public Flotte() {
		bateaux=new ArrayList<>();
	}

	public void ajouterBateau(Bateau b){
		if(!bateaux.contains(b)) {
			bateaux.add(b);
		}
	}

	public Bateau bateauA(int x, int y) {
		for(Bateau bateau : bateaux) {
			if (bateau.contient(x, y)) {
				return bateau;
			}
		}
		return null;
	}

	public boolean tousCoules() {
		boolean fini = true;
		// si un des bateaux n'est pas detruit, 'fini' devient (et reste) false
		for(Bateau bateau : bateaux) {
			fini = fini && bateau.estDetruit();
		}
		return fini;
	}

}
