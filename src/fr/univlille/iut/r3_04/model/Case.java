package fr.univlille.iut.r3_04.model;

public class Case {
	protected int abcisse;
	protected int ordonnee;
	protected ECase etat;
	protected Bassin bassin;

	public Case(int x, int y, Bassin bassin) {
		this.abcisse = x;
		this.ordonnee = y;
		this.bassin = bassin;
		this.etat = ECase.EAU;
	}

	public int getAbcisse(){
		return abcisse;
	}

	public int getOrdonnee(){
		return ordonnee;
	}

	public ECase getEtat(){
		return etat;
	}

	public void setEtat(ECase etat) {
		this.etat = etat;
	}

	public boolean equals(int x, int y) {
		return (abcisse == x) && (ordonnee == y);
	}

}
