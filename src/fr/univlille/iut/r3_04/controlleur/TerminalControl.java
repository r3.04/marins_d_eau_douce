package fr.univlille.iut.r3_04.controlleur;

import java.util.Scanner;

import fr.univlille.iut.r3_04.model.Bassin;

public class TerminalControl {

	protected Bassin bassin;
	protected boolean fini;
	
	public TerminalControl(Bassin bassin) {
		this.bassin = bassin;
		fini = false;
	}
	
	public void run() {
		Scanner scan = new Scanner(System.in);
		while (! fini) {
			int x, y;
			System.out.print("Coordonnees de tir (x y): ");
			x = scan.nextInt();
			y = scan.nextInt();
			
			bassin.tir(x,y);
		}
		scan.close();
	}
	
	public void setFin() {
		fini = true;
	}
}
