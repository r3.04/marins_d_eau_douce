# Marins d'eau douce

Ce petit programme implémente une sorte de bataille navale simpliste
en mode ligne de commande.

Nous allons l'utilisez pour comprendre comment le polymorphisme permet
d'éviter les instructions `if` ou `switch`.

Commencer par lire le code pour comprendre comment il est organisé :
- `fr.univlille.iut.r3_04.Jeu`
la classe principale qui permet de lancer le jeu ;
-  `fr.univlille.iut.r3_04.model.Bassin` la partie principale du modèle (MVC), elle contient le plateau de jeu (de taille paramétrable) et la flotte de bateaux. C'est elle qui gère le résultat d'un `tir(int x, int y)` et stocke ce résultat dans son attribut `dernierCoup`  ;
- `fr.univlille.iut.r3_04.model.Flotte` contient la liste des bateaux, peut retoruver un bateau à partir d'une coordonnée, et vérifier si tous les bateaux sont coulés (fin du jeu) ;
- `fr.univlille.iut.r3_04.model.Bateau` appartient à une flotte et contient la liste des cases du bassin (attribut `elements`) où il est positionné. Il est initialisé à partir d'une position, dans une direction (horizontal ou pas) et avec un type qui donne sont nombre de cases ;
- `fr.univlille.iut.r3_04.model.EBateau` énumération de tous les types de bateaux avec leur taille respective ;
- `fr.univlille.iut.r3_04.model.ECase` énumération des contenu possible d'une case (rien, bateau normal, ou bateau touché) ;
- `fr.univlille.iut.r3_04.model.ECoup` énumération des résultats possibles d'un coup (début=pas encore de coup, touché, coulé, à-l'eau, fin-du-jeu, erreur-de-tir)
- `fr.univlille.iut.r3_04.controlleur.TerminalControl` la classe de controle du MVC, qui boucle pour demander un coup et le transmettre au modèle qui gèrera celui-ci ;
- `fr.univlille.iut.r3_04.view.BassinView` la classe de vue du MVC, qui affiche le résultat d'un coup et/ou la grille de jeu.

Note: La programme n'est pas testé, ce qui pour ce TP est inconvéniant
car nous allons faire des modifications profondes au programme.
Avant chaque exercice, il est recommendé d'écrire quelques tests de la
partie à modifié (qui devront être verts, bien sûr) pour s'assurer
après la modification que le programme fonctionne toujours
correctement.

C'est une bonne façon pour constituer une base de tests dans une
application qui n'en a pas, que de commencer toute évolution du code
par tester ce qui va être modifié.

## 1- Élimination d'un test simple

On voit dans le constructeur de `Bateau` une instruction
`if(horizontal)`.
Comment le polymorphisme peut-il nous permettre de supprimer cette
instruction ?

L'idée est de faire de la direction un objet. Il y aura 2 classes
différentes, une pour la `DirectionHorizontale` et une pour la
`DirectionVerticale`.
Au lieu de passer un booléen au constructeur, on passera un objet de
la classe souhaitée.
Au lieu de tester la valeur de la direction, on appelera une méthode
sur l'objet direction qui aura le bon comportement pour l'horizontale
ou la verticale.

Ici, une solution est de récupéré dans le constructeur la première
case du `Bateau`, puis de demander à la direction de calculer la case
suivante.
Pour chaque case du bateau (sauf la première qui est donnée), on
demandera à l'objet `Direction` de calculer la case suivante à partir
de la case courante.

Pour que cela fonctionne, il faut que les deux classes
`DirectionHorizontale` et `DirectionVerticale` hérite de la même
classe abstraite ou implémente la même interface qui définira la
méthode abstraite `public Case caseSuivante(Case casePrecedente, Bassin bassin)`.
Note: Il faut passer le bassin à la métode car elle en a besoin pour "aller chercher" une case : `bassin.getCase(...)`

finalement dans la classe Jeu, il faut changer la création des bateaux pour passer un objet au lieu d'un booléen.
Pour simplifier un peu l'utilisation, vous pouvez définir une sorte de
"fabrique", avec des méthodes statiques dans la classe abstraite pour
créer chaque objet direction:
```Java
public static Direction verticale() {
  return new DirectionVerticale();
}
```
Qu'on appellera facilement et de manière très lisible par: `bassin.ajouterBateau( new Bateau(EBateau.CROISEUR, 2, 3, Direction.horizontale(), bassin));`.

Note, le fait de créer des objet permet aussi de rajouter
facilement des nouvelles directions, par exemple diagonale1 et
diagonale2.

## 2- Élimination d'un `switch/case`

Dans `Bassin`, la méthode `tir(int x, int y)` est assez complexe avec une "sous-méthode" pour gérer les cas où le tir fait mouche.
La méthode fait un `switch/case` pour savoir ce qu'il y a dans la case où on tire (`etat` de la case: `ECase`) et quel est le résultat dans chaque cas.

De nouveau on peut remplacer cela par des objets de différentes classes (un classe pour chacun des trois états possibles: `EtatEau`, `EtatBateau`, `EtatTouche`).
Ces trois classes devront hériter d'une classe abstraite (ou interface) commune qui définira une méthode pour calculer le résultat d'un coup sur une case ayant cet état: `public ECoup resultatCoup()`.
Remarque: il n'est pas nécessaire d'indiquer à cette méthode où est fait le tir puisque elle est appellée pour un objet état qui appartient à une case donnée (celle où on a tiré).

En exemple, pour la nouvelle classe `EtatEau`, le `resultatCoup()` sera simplement `ECoup.A_L_EAU`.
Pour les deux autres, le résultat est plus complexe à calculer.

Premièrement pour  `EtatBateau`, `EtatTouche`, une bonne idée est de stoker le bateau concerné dans l'état, ainsi on peut vérifier si il est simplement touché ou bien coulé (ce qui change le résultat du coup) Faites attention, il y a un "touché" pour l'état d'une case et un autre pour le résultat du coup. Ne les mélanger pas.
Comme le bateau connait la flotte à laquelle il appartient, on pourra aussi vérifier si tous les bateaux sont coulés (fin de la partie).
Dans le cas ou le tire est sur une case qui contient un bateau,
on dispose donc de toute l'information nécessaire pour décider entre les 3 résultats: `TOUCHE`, `COULE`, et `FINI`.

Vous prendrez garde que l'état de la case doit aussi changer (de `BATEAU` à `TOUCHE`).
Pour cela, il est suggéré que `Bassin` demande à la `Case` du tir quel est le `resultatCoup()`.
Ainsi la case pourra demander à son état actuel: quel est le `etatApresTir()` (que vous implémenterer dans chaqu'une des classes  d'état).
Elle pourra alors se mettre à jour, puis elle demandera à ce nouvel état, le `resultatCoup()` qu'elle retournera au `Bassin`.

De nouveau vous pouvez faire une sorte de "fabrique" dans la classe abstraite des états pour créer facilement chaque état: `AbstractEtatCase.eau()` ou  `AbstractEtatCase.bateau()`, ...

## 3- _Double Dispatch_

Maintenant que l'état des cases est représenté par des objets, on peut s'en servir pour afficher le bassin.

Dans la classe `BassinView`, méthode `show()` l'état d'une case est testé (avec un `switch/case`)  pour savoir comment l'afficher.
Note: Initialisez `BassinView` avec le paramètre `triche` à vrai pour voir l'affichage du bassin.

Une première solution pour faire l'affichage sans le `switch/case`, est de mettre une méthode (`toString` par exemple) dans chaque classe des états qui retourne le caractère à afficher.

Mais cette solution présente un problème: Elle introduit dans le modèle, une considération liée à la vue.
Si on faisait une vue JavaFX pour ce programme, il faudrait changer la méthode d'affichage dans le modèle.

Une meilleure solution serait que ce qui a trait à l'affichage soit dans le paquetage de la vue.
Pour ne pas remettre un `switch/case`, nous allons utiliser le _Double dispatch_:
- Créez une interface `IEtatHandler` qui offre trois méthodes (une pour chaque état possible): `void handleEtatEau(EtatEau etat)`,` void handleEtatTouche(EtatTouche etat)`, void `handleEtatCoule(EtatCoule etat)`.
- Puis dans chaque classe des états vous mettrez une méthode `void handler(IEtatHandler handler)`, qui indique à l'état qu'un objet `handler` est prêt à le manipuler.
- Dans sa méthode `handler`, chaque état appelera en retour ce handler la méthode "handle..." qui lui correspond.
- Finalement l'interface doit être implémentée dans la vue et chacune des trois méthodes fera le bon affichage.

Ainsi :
1. la vue appelle `handler(this)` d'un etat d'une case. Le `this`sert à dire que le "handler" est la propre vue ;
1. l'état appelle en retour dans la vue la méthode qui lui correspond, chaque état sait qu'elle méthode de `IEtatHandler` lui correspond, il ni a aucun choix à faire ;
1. la méthode appellée dans la vue ("handle...") produit l'affichage correct pour la case courante.

Pour plus de généralité, les méthodes "handle..." reçoivent en paramètre l'objet état correspondant.
Ce n'est pas utile ici, mais d'autres "handler" pourraient en avoir besoin.

On voit ainsi que grace au _Double dispatch_, le modèle n'a pas besoin de connaitre la vue. Tout ce qu'il connait c'est une classe qui implémente l'interface `IEtatHandler`.
Sans changer quoique ce soit au modèle, on pourrait parfaitement faire une vue JavaFX  (si elle implémente l'interface).


## 4- Répétez

Vous pouvez refaire les mêmes modifications que les exercices 2 et 3, pour le type énuméré `ECoup`, c'est à dire remplacer ce type énuméré par une petite hiérarchie de classe (une pour chaque résultat possible).

## 5 - Répétez

Pour pratiquer le _Double dispatch_, vous pourvez aussi faire l'exercice du Chifoumi (Pierre/Feuille/Ciseaux) sans utiliser d'instruction `if` :
